Hello World(An Overlay that shows lots and lots of stuff)의 한글화 작업 갤럼입니다.

myconfig내의 개인 커스텀 파일은 몬스터,버프를 제외한 나머지 기능을 off한 파일입니다.
해당 기능 외에 다른 UI기능을 보이게 하려면 해당 파일내의 다른 UI의 enable을 false에서 true로 변경해서 적용 하시길 바랍니다.

Hello World에서 제공하는 파라미터의 정보에 대한 설명은 Parameter Guide&Changing Guide 폴더내에 워드 파일로 작성이 되어 있습니다. 
큰 변경점이 있는 경우(8.6버전같은 경우)에만 따로 해당 버전에서 변경이 있는 내용을 작성하여 별도의 가이드 파일을 작성중입니다.

현재 endemic폴더는 수정을 하지 않았으므로 8.3버전기준으로 적용이 됩니다.
